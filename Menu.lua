local menu_prefix = "gadgets_menu_"
local main_menu_id = "gadgets_menu_main"
local localization_file = ModPath .. "localization/menu.json"
local settings_file = ModPath .. "settings.json"

local theme_names = {
	laser = { "player", "team", "cop_sniper", "turret_module_active", "turret_module_rearming", "turret_module_mad" },
	flashlight = { "player", "team" },
}


Hooks:Add("MenuManagerPopulateCustomMenus", "MenuManagerPopulateCustomMenus_Gadgets", function(self, nodes)
	local item_table = {}
	
	local function change_setting(value, category, setting, gadget)
		local current_theme = Gadgets.current_theme[gadget]
		local theme = theme_names[gadget][current_theme]
		
		Gadgets.settings[gadget][theme][category][setting] = value
		Gadgets.values_changed = true
	end
	
	local function default_value(category, setting, gadget)
		local current_theme = Gadgets.current_theme[gadget]
		local theme = theme_names[gadget][current_theme]
		
		return Gadgets.settings[gadget][theme][category][setting]
	end
	
	local function change_active_theme(value, _, _, gadget)
		Gadgets:save()
		
		Gadgets.current_theme[gadget] = value
		
		for category, settings in pairs(item_table[gadget]) do
			for setting, data in pairs(settings) do
				local value = data.value_clbk(category, setting, gadget)
				
				if data.item_type == "toggle" then
					data.item:set_value(value and true or false)
				elseif data.item_type == "slider" then
					data.item:set_value(value)
				end
				
				local gui_node = data.item:parameters().gui_node
				gui_node:reload_item(data.item)
			end
		end
	end
	

	local function initialize_menu(menu_id, data)
		local prefixed_menu_id = menu_prefix .. menu_id
		item_table[menu_id] = item_table[menu_id] or {}
		
		MenuHelper:NewMenu(prefixed_menu_id)
		
		for i, item in ipairs(data) do
			local setting = item[1]
			local item_type = item[2]
			local item_data = item[3] or {}
			
			local change_clbk = item_data.change_clbk or data.default_change_clbk
			local category = item_data.category or "theme"
			local id = string.format("%s_%s_%s", menu_id, category, setting)
			local clbk_id = string.format("%s_%s_clbk", menu_prefix, id)
			local title = string.format("%s%s_title", menu_prefix, id)
			local desc = string.format("%s%s_desc", menu_prefix, id)
			
			if item_type == "toggle" then
				local default = data.default_value_clbk(category, setting, menu_id)
				MenuHelper:AddToggle({ id = id, title = title, desc = desc, callback = clbk_id, menu_id = prefixed_menu_id, priority = -i, value = default and true or false })
				
				MenuCallbackHandler[clbk_id] = function(self, item)
					return change_clbk(item:value() == "on", category, setting, menu_id)
				end
			elseif item_type == "slider" then
				local default = data.default_value_clbk(category, setting, menu_id)
				MenuHelper:AddSlider({ id = id, title = title, desc = desc, callback = clbk_id, min = item_data.min, max = item_data.max, step = item_data.step, show_value = true, menu_id = prefixed_menu_id, priority = -i, value = default or 0 })
				
				MenuCallbackHandler[clbk_id] = function(self, item)
					if item_data.round then item:set_value(math.round(item:value())) end
					
					return change_clbk(item:value(), category, setting, menu_id)
				end
			elseif item_type == "multichoice" then
				local items = {}
				for _, item in ipairs(item_data.items) do
					table.insert(items, string.format("%s%s", menu_prefix, item))
				end
				
				MenuHelper:AddMultipleChoice({ id = id, title = title, desc = desc, callback = clbk_id, items = items, menu_id = prefixed_menu_id, priority = -i, value = 1 })
				
				MenuCallbackHandler[clbk_id] = function(self, item)
					return change_clbk(item:value(), category, setting, menu_id)
				end
			elseif item_type == "divider" then
				MenuHelper:AddDivider({ id = id, size = item_data.size, menu_id = prefixed_menu_id, priority = -i })
			end
			
			if item_type == "slider" or item_type == "toggle" then
				item_table[menu_id][category] = item_table[menu_id][category] or {}
				local items = MenuHelper:GetMenu(prefixed_menu_id)._items_list
				item_table[menu_id][category][setting] = { item = items[#items], item_type = item_type, value_clbk = data.default_value_clbk }
			end
		end
		
		for sub_menu_id, sub_menu_data in pairs(data.sub_menus or {}) do
			initialize_menu(sub_menu_id, sub_menu_data)
		end
	end
	
	local function finalize_menu(menu_id, data, parent, back_clbk)
		local prefixed_menu_id = menu_prefix .. menu_id
		
		nodes[prefixed_menu_id] = MenuHelper:BuildMenu(prefixed_menu_id, { back_callback = back_clbk })
		MenuHelper:AddMenuItem(nodes[parent], prefixed_menu_id, prefixed_menu_id .. "_title", prefixed_menu_id .. "_desc")
		
		for sub_menu_id, sub_menu_data in pairs(data.sub_menus or {}) do
			finalize_menu(sub_menu_id, sub_menu_data, prefixed_menu_id, back_clbk)
		end
	end
	
	
	--Menu structure
	local main_menu = {
		sub_menus = {
			laser = {
				default_change_clbk = change_setting,
				default_value_clbk = default_value,
				
				--THEME MULTICHOICE
				{ "divider", "divider", { size = 24 }},
				{ "r", "slider", { min = 0, max = 1, step = 0.01, category = "beam" }},
				{ "g", "slider", { min = 0, max = 1, step = 0.01, category = "beam" }},
				{ "b", "slider", { min = 0, max = 1, step = 0.01, category = "beam" }},
				{ "a", "slider", { min = 0, max = 1, step = 0.01, category = "beam" }},
				{ "divider", "divider", { size = 12 }},
				{ "match_beam", "toggle", { category = "dot" }},
				{ "r", "slider", { min = 0, max = 1, step = 0.01, category = "dot" }},
				{ "g", "slider", { min = 0, max = 1, step = 0.01, category = "dot" }},
				{ "b", "slider", { min = 0, max = 1, step = 0.01, category = "dot" }},
				{ "a", "slider", { min = 0, max = 1, step = 0.01, category = "dot" }},
				{ "divider", "divider", { size = 12 }},
				{ "match_beam", "toggle", { category = "glow" }},
				{ "r", "slider", { min = 0, max = 1, step = 0.01, category = "glow" }},
				{ "g", "slider", { min = 0, max = 1, step = 0.01, category = "glow" }},
				{ "b", "slider", { min = 0, max = 1, step = 0.01, category = "glow" }},
				{ "a", "slider", { min = 0, max = 1, step = 0.01, category = "glow" }},
				{ "divider", "divider", { size = 18 }},
				{ "enabled", "toggle", { category = "rainbow" }},
				{ "frequency", "slider", { min = 0, max = 5, step = 0.1, category = "rainbow" }},
				{ "divider", "divider", { size = 18 }},
				{ "enabled", "toggle", { category = "pulse" }},
				{ "min", "slider", { min = 0, max = 5, step = 0.1, category = "pulse" }},
				{ "max", "slider", { min = 0, max = 5, step = 0.1, category = "pulse" }},
				{ "frequency", "slider", { min = 0, max = 5, step = 0.1, category = "pulse" }},
			},
			flashlight = {
				default_change_clbk = change_setting,
				default_value_clbk = default_value,
				
				--THEME MULTICHOICE
				{ "divider", "divider", { size = 24 }},
				{ "r", "slider", { min = 0, max = 1, step = 0.01, category = "light" }},
				{ "g", "slider", { min = 0, max = 1, step = 0.01, category = "light" }},
				{ "b", "slider", { min = 0, max = 1, step = 0.01, category = "light" }},
				{ "divider", "divider", { size = 12 }},
				{ "brightness", "slider", { min = 0, max = 5, step = 0.1, category = "light" }},
				{ "angle", "slider", { min = 0, max = 160, step = 5, category = "light" }},
				{ "range", "slider", { min = 0, max = 100, step = 5, category = "light" }},
				{ "divider", "divider", { size = 18 }},
				{ "enabled", "toggle", { category = "rainbow" }},
				{ "frequency", "slider", { min = 0, max = 5, step = 0.1, category = "rainbow" }},
				{ "divider", "divider", { size = 18 }},
				{ "enabled", "toggle", { category = "pulse" }},
				{ "min", "slider", { min = 0, max = 5, step = 0.1, category = "pulse" }},
				{ "max", "slider", { min = 0, max = 5, step = 0.1, category = "pulse" }},
				{ "frequency", "slider", { min = 0, max = 5, step = 0.1, category = "pulse" }},
			},
		},
	}
	table.insert(main_menu.sub_menus.laser, 1, { "theme", "multichoice", { items = theme_names.laser, change_clbk = change_active_theme }})
	table.insert(main_menu.sub_menus.flashlight, 1, { "theme", "multichoice", { items = theme_names.flashlight, change_clbk = change_active_theme }})
	
	
	local back_clbk = menu_prefix .. "back_clbk"
	MenuCallbackHandler[back_clbk] = function(self, item)
		Gadgets:save()
	end
	
	if nodes.blt_options then
		initialize_menu("main", main_menu)
		finalize_menu("main", main_menu, "blt_options", back_clbk)
	end
end)

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_Gadgets_localization", function(self)
	LocalizationManager:load_localization_file(localization_file)
end)







if not Gadgets then
	Gadgets = {
		current_theme = {
			laser = 1,
			flashlight = 1,
		},
		
		settings = {
			laser = {
				player = { 
					beam = { r = 0, g = 1, b = 0, a = 0.15 },
					glow = { match_beam = true, r = 0, g = 1, b = 0, a = 0.02 },
					dot = { match_beam = true, r = 0, g = 1, b = 0, a = 1 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
				team = { 
					beam = { r = 0, g = 1, b = 0, a = 0.05 },
					glow = { match_beam = true, r = 0, g = 1, b = 0, a = 0.02 },
					dot = { match_beam = true, r = 0, g = 1, b = 0, a = 1 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
				cop_sniper = { 
					beam = { r = 1, g = 0, b = 0, a = 0.15 },
					glow = { match_beam = true, r = 1, g = 0, b = 0, a = 0.02 },
					dot = { match_beam = true, r = 1, g = 0, b = 0, a = 1 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
				turret_module_active = { 
					beam = { r = 1, g = 0, b = 0, a = 0.15 },
					glow = { match_beam = true, r = 1, g = 0, b = 0, a = 0.02 },
					dot = { match_beam = true, r = 1, g = 0, b = 0, a = 1 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
				turret_module_rearming = { 
					beam = { r = 1, g = 1, b = 0, a = 0.11 },
					glow = { match_beam = true, r = 1, g = 1, b = 0, a = 0.02 },
					dot = { match_beam = true, r = 1, g = 1, b = 0, a = 1 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
				turret_module_mad = { 
					beam = { r = 0, g = 1, b = 0, a = 0.15 },
					glow = { match_beam = true, r = 0, g = 1, b = 0, a = 0.02 },
					dot = { match_beam = true, r = 0, g = 1, b = 0, a = 1 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
			},
			flashlight = {
				player = {
					light = { r = 1, g = 1, b = 1, brightness = 1, range = 10, angle = 60 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
				team = {
					light = { r = 1, g = 1, b = 1, brightness = 1, range = 10, angle = 60 },
					pulse = { enabled = false, min = 0.5, max = 2, frequency = 0.25 },
					rainbow = { enabled = false, frequency = 0.25 },
				},
			},
		},
	}
	
	function Gadgets:save(force)
		if not (self.values_changed or force) then return end
		
		self.values_changed = nil
	
		local file = io.open(settings_file, "w+")
		if file then
			file:write(json.encode(self.settings))
			file:close()
		end
		
		self:_update_themes()
	end
	
	function Gadgets:load()
		local file = io.open(settings_file, "r")
		if file then
			self.settings = json.decode(file:read("*all"))
			file:close()
		else
			Gadgets:save(true)
		end
		
		self:_update_themes()
	end
	
	function Gadgets:_update_themes()
		if WeaponGadgetBase then
			self:update_gadget_themes("laser")
			self:update_gadget_themes("flashlight")
		end
	end
	
	function Gadgets:update_gadget_themes(type)
		WeaponGadgetBase.THEME_SETTINGS[type] = self.settings[type]
		
		for key, unit in pairs(WeaponGadgetBase.SPAWNED_UNITS[type]) do
			unit:base():refresh_themes()
		end
	end
	
	Gadgets:load()
end