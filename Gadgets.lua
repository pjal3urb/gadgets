printf = printf or function(...) log(string.format(...)) end

if RequiredScript == "lib/units/weapons/weapongadgetbase" then
	
	WeaponGadgetBase.SPAWNED_UNITS = {
		laser = {},
		flashlight = {},
	}
	
	--TODO: These needs to be filled by default if no option menu is used
	WeaponGadgetBase.THEME_SETTINGS = {
		laser = {
			player = {},
			team = {},
			cop_sniper = {},
			turret_module_active = {},
			turret_module_rearming = {},
			turret_module_mad = {},
		},
		flashlight = {
			player = {},
			team = {},
		},
	}
	
	local init_original = WeaponGadgetBase.init
	local destroy_original = WeaponGadgetBase.destroy
	
	function WeaponGadgetBase:init(...)
		init_original(self, ...)
		
		if WeaponGadgetBase.SPAWNED_UNITS[self.GADGET_TYPE] then
			WeaponGadgetBase.SPAWNED_UNITS[self.GADGET_TYPE][self._unit:key()] = self._unit
		end
	end
	
	function WeaponGadgetBase:destroy(...)
		if WeaponGadgetBase.SPAWNED_UNITS[self.GADGET_TYPE] then
			WeaponGadgetBase.SPAWNED_UNITS[self.GADGET_TYPE][self._unit:key()] = nil
		end
		
		destroy_original(self, ...)
	end
	
	
	function WeaponGadgetBase:set_owner_unit(owner)
		if alive(owner) then
			--printf("Setting %s unit %s owner (slot %d)", tostring(self.GADGET_TYPE), tostring(self._unit:key()), owner:slot())
			if managers.criminals:character_data_by_unit(owner) then
				self:_set_theme("team")
			elseif owner == managers.player:player_unit() then
				self:_set_theme("player")
			--elseif owner:in_slot(12) then
				--Sniper, theme set at unit creation
			--elseif owner:in_slot(25) or owner:in_slot(26) then
				--Sentry/Turret, theme switched during operation for turrets
				--local user_type = table.contains(managers.groupai:state():turrets() or {}, owner) and "turret" or "sentry"
			--else
				--printf("UNKNOWN OWNER SLOT %d", owner:slot())
			end
		end
	end
	
	function WeaponGadgetBase:refresh_themes()
	
	end
	
	function WeaponGadgetBase:_set_theme(theme)
	
	end
	
	function WeaponGadgetBase:_update_effects(data, t, dt)
		if data then
			local color
			
			if data.rainbow then
				local r = t * 360 * data.rainbow.frequency
				color = Vector3((1 + math.sin(r + 0)) / 2, (1 + math.sin(r + 120)) / 2, (1 + math.sin(r + 240)) / 2)
				self:set_color(color)
			end
			
			if data.pulse then
				local r = 0.5 + 0.5 * math.sin(t * 180 * data.pulse.frequency)
				local intensity = math.lerp(data.pulse.min, data.pulse.max, r)
				self:_set_pulse_intensity(intensity, color)
			end
		end
	end
	
end

if RequiredScript == "lib/units/weapons/weaponlaser" then
	
	if Gadgets then
		Gadgets:update_gadget_themes("laser")
	end
	
	local init_original = WeaponLaser.init
	local update_original = WeaponLaser.update
	
	function WeaponLaser:init(...)
		init_original(self, ...)
		self._theme_type = "player"
		self:refresh_themes()
	end
	
	function WeaponLaser:update(unit, t, dt, ...)
		update_original(self, unit, t, dt, ...)
		self:_update_effects(self._themes[self._theme_type], t, dt)
	end
	
	function WeaponLaser:set_color(color)
		local tmp = Vector3(color:unpack())
		self:_set_colors(tmp, tmp, tmp)
	end
	
	function WeaponLaser:set_color_by_theme(type)
		if not self._themes[type] then printf("ERROR (WeaponLaser:set_color_by_theme): Attempting to set missing theme %s", tostring(type)) end
		
		self._theme_type = type
		self:_set_colors()
	end
	
	
	function WeaponLaser:refresh_themes()
		for theme, data in pairs(WeaponGadgetBase.THEME_SETTINGS.laser) do
			local beam = Vector3(data.beam.r, data.beam.g, data.beam.b)
		
			self._themes[theme] = {
				light = data.dot.match_beam and beam or Vector3(data.dot.r, data.dot.g, data.dot.b),
				glow = data.glow.match_beam and beam or Vector3(data.glow.r, data.glow.g, data.glow.b),
				brush = Vector3(data.beam.r, data.beam.g, data.beam.b),
				alpha = { dot = data.dot.a, glow = data.glow.a, beam = data.beam.a },
				rainbow = data.rainbow.enabled and {
					frequency = data.rainbow.frequency,
				},
				pulse = data.pulse.enabled and {
					min = data.pulse.min, 
					max = data.pulse.max, 
					frequency = data.pulse.frequency,
				},
			}
		end

		self._current_intensity = 1
		self:_set_theme(self._theme_type)
	end
	
	function WeaponLaser:_set_theme(theme)
		self:set_color_by_theme(theme)
	end
	
	function WeaponLaser:_set_pulse_intensity(intensity, color)
		self._current_intensity = intensity
		self:_set_colors(color, color, color)
	end
	
	function WeaponLaser:_set_colors(light, glow, brush)
		local theme = self._themes[self._theme_type] or self._themes.default
		local alpha = theme and theme.alpha or { dot = 1, glow = 0.02, beam = 0.15 }
		
		mvector3.set(self._light_color, (light or theme.light) * 10 * alpha.dot * self._current_intensity)
		mvector3.set(self._light_glow_color, (glow or theme.glow) * 10 * alpha.glow * self._current_intensity)
		self._brush_color = Color((brush or theme.brush):unpack()):with_alpha(alpha.beam * self._current_intensity)
		
		self._light:set_color(self._light_color)
		self._light_glow:set_color(self._light_glow_color)
		self._brush:set_color(self._brush_color)
	end
	
end

if RequiredScript == "lib/units/weapons/weaponflashlight" then
	
	if Gadgets then
		Gadgets:update_gadget_themes("flashlight")
	end
	
	local init_original = WeaponFlashLight.init
	local update_original = WeaponFlashLight.update
	
	WeaponFlashLight._themes = {}
	
	function WeaponFlashLight:init(...)
		init_original(self, ...)
		self._theme_type = "player"
		self:refresh_themes()
	end
	
	function WeaponFlashLight:update(unit, t, dt, ...)
		update_original(self, unit, t, dt, ...)
		
		if not self:is_haunted() then
			self:_update_effects(self._themes[self._theme_type], t, dt)
		end
	end
	
	
	function WeaponFlashLight:refresh_themes()
		for theme, data in pairs(WeaponGadgetBase.THEME_SETTINGS.flashlight) do
			self._themes[theme] = {
				brightness = data.light.brightness,
				light = Vector3(data.light.r, data.light.g, data.light.b),
				angle = data.light.angle,
				range = data.light.range * 100,
				rainbow = data.rainbow.enabled and {
					frequency = data.rainbow.frequency,
				},
				pulse = data.pulse.enabled and {
					min = data.pulse.min, 
					max = data.pulse.max, 
					frequency = data.pulse.frequency,
				}
			}
		end
		
		self._current_intensity = 1
		self:_set_theme(self._theme_type)
	end
	
	function WeaponFlashLight:set_color(color)
		self:_set_colors(color)
	end
	
	function WeaponFlashLight:_set_theme(theme)
		self._theme_type = theme
		
		if not self:is_haunted() then
			local theme = self._themes[self._theme_type]
			
			self:_set_colors(theme.light)
			self._light:set_spot_angle_end(theme.angle)
			self._light:set_far_range(theme.range)
		end
	end
	
	function WeaponFlashLight:_set_pulse_intensity(intensity, color)
		self._current_intensity = intensity
		self:_set_colors(color)
	end
	
	function WeaponFlashLight:_set_colors(light)
		local theme = self._themes[self._theme_type]
		local light = light or theme.light
		self._light:set_color(light * theme.brightness * self._current_intensity)
	end
	
end
	
if RequiredScript == "lib/units/weapons/raycastweaponbase" then

	local setup_original = RaycastWeaponBase.setup

	function RaycastWeaponBase:setup(...)
		setup_original(self, ...)
		self:_update_gadget_owner(self._has_gadget)
	end
	
	function RaycastWeaponBase:_update_gadget_owner(gadgets)
		if gadgets then
			for _, id in ipairs(gadgets) do
				local base = self._parts[id] and alive(self._parts[id].unit) and self._parts[id].unit:base()
				if base and base.set_owner_unit then
					base:set_owner_unit(self._setup.user_unit)
				end
			end
		end
		
		--if alive(self._second_gun) then
		--	local base = self._second_gun:base()
		--	base:_update_gadget_owner(managers.weapon_factory:get_parts_from_weapon_by_type_or_perk("gadget", base._factory_id, base._blueprint))
		--end
	end

end

if RequiredScript == "lib/units/weapons/newnpcraycastweaponbase" then

	local setup_original = NewNPCRaycastWeaponBase.setup

	function NewNPCRaycastWeaponBase:setup(...)
		setup_original(self, ...)
		self:_update_gadget_owner(managers.weapon_factory:get_parts_from_weapon_by_type_or_perk("gadget", self._factory_id, self._blueprint))
	end

end
	
if RequiredScript == "lib/units/weapons/npcraycastweaponbase" then

	local set_laser_enabled_original = NPCRaycastWeaponBase.set_laser_enabled

	function NPCRaycastWeaponBase:set_laser_enabled(...)
		set_laser_enabled_original(self, ...)
		
		if alive(self._laser_unit) then
			self._laser_unit:base():set_owner_unit(self._setup.user_unit)
		end
	end

end

if RequiredScript == "lib/units/weapons/sentrygunweapon" then

	local _set_laser_state_original = SentryGunWeapon._set_laser_state

	function SentryGunWeapon:_set_laser_state(...)
		_set_laser_state_original(self, ...)
		
		if alive(self._laser_unit) then
			self._laser_unit:base():set_owner_unit(self._unit)
		end
	end
	
end
